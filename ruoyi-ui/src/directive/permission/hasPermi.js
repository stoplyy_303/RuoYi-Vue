/*
 * @Date: 2021-05-05 11:19:47
 * @LastEditors: Liuyangyang
 * @LastEditTime: 2021-05-05 14:40:41
 */
/**
* 操作权限处理
* Copyright (c) 2019 ruoyi
*/

import store from '@/store'

export default {
  inserted(el, binding, vnode) {
    const { value } = binding
    const all_permission = "*:*:*";
    const permissions = store.getters && store.getters.permissions

    if (value && value instanceof Array && value.length > 0) {
      const permissionFlag = value
      //todo:lyy
      // const hasPermissions = permissions.some(permission => {
      //   return all_permission === permission || permissionFlag.includes(permission)
      // })
      const hasPermissions = true
      if (!hasPermissions) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else {
      throw new Error(`请设置操作权限标签值`)
    }
  }
}
